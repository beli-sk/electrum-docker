Electrum docker image
=====================

Docker image of [Electrum][] bitcoin wallet.

[Electrum]: https://electrum.org


Locations
---------

[Source of this image][source] is hosted on Gitlab.com.

If you find any problems, please [post an issue][issues].

The image can be [pulled from][registry] Docker Hub registry.

[source]: https://gitlab.com/beli-sk/electrum-docker
[issues]: https://gitlab.com/beli-sk/electrum-docker/issues
[registry]: https://hub.docker.com/r/beli/electrum/


Pull or build
-------------

The image is built automatically and transparently with [Gitlab CI
pipeline][pipeline], pushed to [Docker Hub registry][registry] and can be
pulled using command

    docker pull beli/electrum

or if you'd prefer to build it yourself from the source repository

    git clone https://gitlab.com/beli-sk/electrum-docker.git
    cd electrum-docker/
    docker build -t beli/electrum .

[pipeline]: https://gitlab.com/beli-sk/electrum-docker/pipelines
[registry]: https://hub.docker.com/r/beli/electrum/


Usage
-----

The container uses volume `/electrum` as Electrum data directory.  The volumes
should be owned and writable by UID 1000, GID 1000.

The container exposes TCP port 10000 for [Xpra](https://xpra.org/).

Run the container in the background

    docker run -d -p 10000:10000 beli/electrum

and use Xpra on the host to attach to container's GUI

    xpra attach tcp:localhost:10000

